#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 16:33:37 2022

@author: milan
"""
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import (
    Dense,
    Conv2D,
    MaxPool2D,
    Flatten,
    Dropout,
    BatchNormalization,
)


#physical_devices = tf.config.list_physical_devices('GPU') 
#tf.config.experimental.set_memory_growth(physical_devices[0], True)

base_model = keras.applications.VGG16(
    weights='imagenet',  # Load weights pre-trained on ImageNet.
    input_shape=(128, 128, 3),
    include_top=False)

base_model.summary()
base_model.trainable = False

inputs = keras.Input(shape=(128, 128, 3))
# Separately from setting trainable on the model, we set training to False 
x = base_model(inputs, training=False)
x = keras.layers.GlobalAveragePooling2D()(x)
# A Dense classifier with a single unit (binary classification)
outputs = outputs = keras.layers.Dense(1)(x)
model = keras.Model(inputs, outputs)
model.summary()

# Important to use binary crossentropy and binary accuracy as we now have a binary classification problem
model.compile(loss=keras.losses.BinaryCrossentropy(from_logits=True), metrics=[keras.metrics.BinaryAccuracy()])

from tensorflow.keras.preprocessing.image import ImageDataGenerator

# Create a data generator
datagen_train = ImageDataGenerator(
    samplewise_center=True,  # set each sample mean to 0
    rotation_range=10,  # randomly rotate images in the range (degrees, 0 to 180)
    zoom_range=0.1,  # Randomly zoom image
    width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
    height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
    horizontal_flip=True,  # randomly flip images
    vertical_flip=False,
    validation_split=0.2
)  # we don't expect Bo to be upside-down so we will not flip vertically
batch_size = 8
train_it = datagen_train.flow_from_directory(
    "Pistachio_Image_Dataset/Pistachio_Image_Dataset",
    target_size=(128, 128),
    color_mode="rgb",
    class_mode="binary",
    batch_size=batch_size,
    subset='training'
)

# load and iterate validation dataset
valid_it = datagen_train.flow_from_directory(
    "Pistachio_Image_Dataset/Pistachio_Image_Dataset",
    target_size=(128, 128),
    color_mode="rgb",
    class_mode="binary",
    batch_size=batch_size,
    subset='validation'
)

model.fit(train_it, steps_per_epoch=train_it.samples/batch_size, validation_data=valid_it, validation_steps=valid_it.samples/batch_size, epochs=20)

# Unfreeze the base model
base_model.trainable = True

# It's important to recompile your model after you make any changes
# to the `trainable` attribute of any inner layer, so that your changes
# are taken into account
model.compile(optimizer=keras.optimizers.RMSprop(learning_rate = .00001),  # Very low learning rate
              loss=keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=[keras.metrics.BinaryAccuracy()])

model.fit(train_it, steps_per_epoch=train_it.samples/batch_size, validation_data=valid_it, validation_steps=valid_it.samples/batch_size, epochs=10)